﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Konyisoft.Driving
{
	public class PlayerManager : Singleton<PlayerManager>
	{
		#region Enumerations
		
		public enum State
		{
			Stopped,
			Moving,
			Crashed
		}

		#endregion
		
		#region Fields and properties
		
		public Vector3 Velocity // = look direction * speed
		{
			get
			{
				return (currentRotation * Vector3.forward) * currentSpeed;
			}
		}

		public float CurrentSpeed
		{
			get { return currentSpeed; }
		}

		public float DistanceTraveled
		{
			get { return distanceTraveled; }
		}

		public bool IsMoving
		{
			get { return currentSpeed > 0; }
		}

		public string DebugString
		{
			get { return GetDebugString(); }
		}
		
		public Camera playerCamera;
		public float cameraHeight = 1.5f;
		public float yawSpeed = 10f;
		public float yawDamping = 10f;
		public float maxSpeed = 50f; // in m/s
		public float acceleration = 3f;
		public float deceleration = 6f;
		public float speedDamping = 1f;

		float currentYaw;
		float currentSpeed;
		float distanceTraveled;
		Quaternion currentRotation;
		RoadSegment currentSegment;
		RoadSegment lastSegment;
		State currentState = State.Stopped;
		Shaker shaker;
		
		#endregion

		#region Mono methods

		void Awake()
		{
			if (playerCamera == null)
			{
				Debug.LogError("No camera attached.");
				return;
			}
			
			// For camera shaking on crash
			shaker = playerCamera.GetComponent<Shaker>();
		}

		void Start()
		{
			RoadManager.Instance.MoveToOrigin();
			SetStartPosition();
			SetStartRotation();
		}

		void LateUpdate()
		{
			// No camera attached
			if (playerCamera == null)
				return;

			// Currently being crashed?
			if (InState(State.Crashed))
			{
				// Back to the center of the road when pressing Space
				if (lastSegment != null && Input.GetKeyUp(KeyCode.Space))
				{
					BackToTheRoad();
				}
				else
				{
					return;
				}
			}

			float deltaTime = Time.deltaTime;

			// Do a raycast against the segment below the camera
			RaycastHit? hit = RaycastSegment();
			if (!hit.HasValue && InState(State.Moving))
			{
				Crash();
				return;
			}
			
			// Current rotation
			currentRotation = playerCamera.transform.localRotation; // by defult
			
			// Has hit on segment
			if (hit.HasValue)
			{
				// Roll
				Vector3 lookDirection = ((hit.Value.point + new Vector3(0, cameraHeight, 0)) - playerCamera.transform.position).normalized;
				currentRotation = Quaternion.LookRotation(lookDirection);

				// Changing segments?
				if (currentSegment == null || hit.Value.transform != currentSegment.transform)
				{
					// We have just left the current segment, update it!
					if (currentSegment != null)
					{
						RoadManager.Instance.UpdateSegment(currentSegment);
					}
					currentSegment = hit.Value.transform.GetComponent<RoadSegment>();
				}
				lastSegment = currentSegment;
			}
			else
			{
				currentSegment = null;
				return;
			}

			// Up (W)
			if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
			{
				currentSpeed += acceleration * deltaTime;
			}
			// Down (S)
			else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
			{
				currentSpeed -= deceleration * deltaTime;
			}
			else if (currentSpeed > 0)
			{
				currentSpeed -= speedDamping * deltaTime;
			}

			// Keep the speed between min and max values
			currentSpeed = Mathf.Clamp(currentSpeed, 0, maxSpeed);

			// Set current state based on the speed value
			if (currentSpeed <= 0)
			{
				SetState(State.Stopped);
			}
			else
			{
				SetState(State.Moving);
			}
				
			// Left (A)
			if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
			{
				currentYaw = -1f;
			}
			// Right (D)
			else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
			{
				currentYaw = 1f;
			}
			
			// Yaw speed modified by move speed
			float relativeYawSpeed = yawSpeed * MathUtils.Normalize(currentSpeed, 0, maxSpeed) * 10f;
			relativeYawSpeed = Mathf.Clamp(relativeYawSpeed, 0, yawSpeed);

			// Yaw
			currentRotation *= Quaternion.Euler(0, currentYaw * relativeYawSpeed * deltaTime, 0);
			
			// Apply calculated rotation smoothly
			playerCamera.transform.localRotation = Quaternion.Lerp(
				playerCamera.transform.localRotation,
				currentRotation,
				deltaTime * 10f
			);
			
			// Apply rotation immediately
			//playerCamera.transform.localRotation = currentRotation;
			
			// Damping
			currentYaw = Mathf.Lerp(currentYaw, 0, yawDamping * deltaTime);

			// Counting distance
			float delta = currentSpeed * deltaTime;
			distanceTraveled += delta;
		}
		
		#endregion

		#region Private methods

		void SetStartPosition()
		{
			playerCamera.transform.position = new Vector3(0, cameraHeight, 0);
		}

		void SetStartRotation()
		{
			List<RoadSegment> segments = RoadManager.Instance.Segments;
			if (segments.Count > 0)
			{
				Vector3 direction = segments[0].StartDirection;
				Quaternion lookRotation = Quaternion.LookRotation(direction, Vector3.up);
				playerCamera.transform.localRotation = currentRotation = lookRotation;
			}
		}

		RaycastHit? RaycastSegment()
		{
			RaycastHit? result = null;
			
			Vector3 rayOrigin = playerCamera.transform.position;
			rayOrigin += playerCamera.transform.forward; // 1m distance in front of the camera
			
			RaycastHit hit;
			Ray ray = new Ray(rayOrigin, Vector3.down);
			
			if (Physics.Raycast(ray, out hit, Mathf.Infinity) && hit.transform.gameObject.tag == "Segment")
			{
				result = hit;
			}
			
			return result;
		}
		
		bool InState(State state)
		{
			return currentState == state;
		}

		void SetState(State state)
		{
			currentState = state;
		}
		
		void Crash()
		{
			currentSpeed = 0;
			if (shaker != null)
				shaker.Shake();
			SetState(State.Crashed);			
		}

		void BackToTheRoad()
		{
			if (shaker != null)
				shaker.Stop();

			// Camera and segments position (XZ)
			Vector3 cameraPositionXZ = VectorUtils.VectorXZ(playerCamera.transform.position);
			Vector3 segmentPositionXZ = VectorUtils.VectorXZ(lastSegment.transform.position);
			// Direction from last segment's pivot to camera position
			Vector3 direction = (cameraPositionXZ - segmentPositionXZ).normalized;
			// Distance of last segment's pivot and camera position
			float distance = Vector3.Distance(cameraPositionXZ, segmentPositionXZ);
			// Translate all segments
			Vector3 translation = direction * (distance - lastSegment.CurveRadius);
			RoadManager.Instance.TranslateSegments(translation);
			
			// Update camera rotation (perpendicular to 'direction' on XZ)
			Vector3 rotationDirection = Vector3.Cross(-direction * lastSegment.CurveDirection, Vector3.up).normalized;
			playerCamera.transform.localRotation = currentRotation = Quaternion.LookRotation(rotationDirection, Vector3.up);
			currentYaw = 0;

			// Camera roll
			RaycastHit? hit = RaycastSegment();
			if (hit.HasValue)
			{
				Vector3 lookDirection = ((hit.Value.point + new Vector3(0, cameraHeight, 0)) - playerCamera.transform.position).normalized;
				playerCamera.transform.localRotation = currentRotation = Quaternion.LookRotation(lookDirection);
			}
			
			SetState(State.Stopped);
		}

		string GetDebugString()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("State: " + currentState.ToString());
			sb.AppendLine("Dist: " + distanceTraveled);
			sb.AppendLine("Speed m/s: " + currentSpeed);
			sb.AppendLine("Speed km/h: " + currentSpeed * Constants.KmPerHourMultiplier);
			sb.AppendLine("Current: " + (currentSegment == null ? "null" : currentSegment.gameObject.name));
			sb.AppendLine("Last: " + (lastSegment == null ? "null" : lastSegment.gameObject.name));			
			return sb.ToString();
		}

		#endregion
	}
}
