﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Driving
{
	public class RoadManager : Singleton<RoadManager>
	{
		#region Fields and properties

		public List<RoadSegment> Segments
		{
			get { return segments; }
		}

		public int segmentCount;
		public RoadSegment segmentPrefab;

		List<RoadSegment> segments;
		
		#endregion
		
		#region Mono methods

		void Awake()
		{
			if (segmentPrefab == null)
			{
				Debug.LogError("No segment prefab attached.");
				return;
			}

			transform.position = Vector3.zero;
			GenerateSegments();
		}

		void Update()
		{
			if (!PlayerManager.Instance.IsMoving)
				return;

			// Move all segments towards the player (opposite direction of player forward)
			for (int i = 0; i < segments.Count; i++)
			{
				segments[i].transform.Translate(-PlayerManager.Instance.Velocity * Time.deltaTime, Space.World);
			}
		}

		#endregion

		#region Public methods

		public void MoveToOrigin()
		{
			if (segments.Count > 0)
				transform.localPosition = new Vector3(0, 0, -segments[0].CurveRadius);
		}

		public void UpdateSegment(RoadSegment segment)
		{
			if (segment != null)
			{
				segments.Remove(segment);
				segment.SetDefaultValues(); // need this?
				segment.Generate(segments[segments.Count - 1]);
				segments.Add(segment);
			}
		}
		
		public void TranslateSegments(Vector3 translation)
		{
			// Do a translation on all segments
			for (int i = 0; i < segments.Count; i++)
			{
				segments[i].transform.Translate(translation, Space.World);
			}
		}

		#endregion

		#region Private methods

		void GenerateSegments()
		{
			segments = new List<RoadSegment>();
			for (int i = 0; i < segmentCount; i++)
			{
				RoadSegment segment = Instantiate(segmentPrefab).GetComponent<RoadSegment>();
				segment.transform.SetParent(transform, false);
				segment.gameObject.name = "Segment " + i;
				segment.Generate(i == 0 ? null : segments[i - 1]);
				segments.Add(segment);
			}
		}

		#endregion
	}
}
