# Midnight Driving
**Konyisoft, 2018**

Procedural road generation for infinite racing games.  
Example/prototype project.  
Created with Unity3D 2017.4 on Ubuntu.

![Screenshot](md.jpg)

Try it out in browser: [WebGL build](http://konyisoft.gitlab.io/midnight-driving/)
